package ru.java.rush.service;

import org.springframework.stereotype.Service;
import ru.java.rush.entity.ProductEntity;
import ru.java.rush.repository.ProductRepository;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void saveAll(List<ProductEntity> list) {
        productRepository.saveAll(list);
    }

    public List<ProductEntity> findAll() {
        return productRepository.findAll();
    }
}
